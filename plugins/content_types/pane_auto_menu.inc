<?php
/**
 * @file
 * Definition of the automated menu panel pane.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Automatic Menu'),
  'description' => t('Generates a menu based on current panel.'),
  'category' => t('Menus'),
  'admin info' => 'pane_auto_menu_pane_admin_info',
  'edit form' => 'pane_auto_menu_pane_edit_form',
  'render callback' => 'pane_auto_menu_pane_render',
  'all contexts' => TRUE,
);

/**
 * Renders the menu.
 */
function pane_auto_menu_pane_render($subtype, $conf, $args, $contexts) {

  // Block.
  $block = new stdClass();
  $block->title = t('Menu');

  // If built, adds menu to the content.
  if (isset($conf['automated_menu']) && $conf['automated_menu']['active']) {
    $block->content = $conf['automated_menu']['auto_id'];
  }

  // Returns rendered content.
  return $block;

}

/**
 * Edit form for our pane.
 */
function pane_auto_menu_pane_edit_form($form, &$form_state) {

  // Current display.
  $display = $form_state['display'];

  // Loads layout plugin.
  $plugin = ctools_get_plugins('panels', 'layouts', $display->layout);

  // If we got any plugin...
  if (!empty($plugin)) {

    // Adds region selector.
    $form['region'] = array(
      '#default_value' => $form_state['conf']['render_from_region'],
      '#description' => t('Choose one of the available regions in your panel display. Keep in mind that if you change the layout, you will need to update the region, otherwise, no content will be rendered.'),
      '#options' => array('' => t('Select a Region')),
      '#required' => TRUE,
      '#title' => t('Region'),
      '#type' => 'select',
    );

    // Adds the options to choose a region.
    foreach ($plugin['regions'] as $region_id => $region_name) {
      $form['region']['#options'][$region_id] = $region_name;
    }

  }

  return $form;

}

/**
 * Submit handler for the editing form.
 */
function pane_auto_menu_pane_edit_form_submit($form, $form_state) {
  $form_state['conf']['render_from_region'] = $form_state['values']['region'];
}

/**
 * Content display for admins on pane editing form.
 */
function pane_auto_menu_pane_admin_info($subtype, $conf, $contexts) {

  // If configuration is given...
  if (!empty($conf)) {

    $block = new stdClass();
    $block->title = t('Region based menu');
    $block->content = t('Generating menu from region !region', array('!region' => '<em>' . $conf['render_from_region'] . '</em>'));
    return $block;

  }

}
